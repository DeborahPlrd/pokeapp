POKEMON APPLICATION


Fonctionnalités :

- Chercher des Pokémons (par nom, en anglais, ex : "charizard" "eevee" "pikachu" "bulbasaur") via le Pokedex (bouton sur la page Home) pour voir ses stats.
Il faut taper le nom entier du Pokémon, puis submit pour lancer la recherche.

- Ajouter des Pokémons à son équipe ("Add to team" une fois la recherche effectuée).
Il est possible d'avoir plusieurs fois le même Pokémon dans son équipe, comme dans les jeux (une équipe de 10 Pikachus est possible par exemple).

- Consulter son équipe (bouton "My team" sur la page d'accueil).
Dans cette équipe on peut supprimer un Pokémon (via la croix) ou vider son équipe (bouton "Clear").
On peut également consulter la fiche d'un Pokémon en cliquant dessus.

- On peut revenir a la Home à tout moment avec le bouton en haut a droite des pages
