import 'package:flutter/material.dart';

class PokeappHomeButton extends StatelessWidget {
  const PokeappHomeButton(this.background, this.text, this.route, {Key? key}) : super(key: key);
  final String background;
  final String text;
  final String route;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 30.0, left: 10, right: 10),
      child: MaterialButton(
        textColor: Colors.white,
        child: Container(
          height: 135,
          padding: const EdgeInsets.all(30),
          decoration:  BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 7,
              ),
            ],
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            image: DecorationImage(
                image: AssetImage(background),
                fit: BoxFit.cover),
          ),
          child: Align(
              alignment: Alignment.bottomRight,
              child: Text(text, style: const TextStyle(fontSize: 20, letterSpacing: 2),)
          ),
        ),
        onPressed:() {
          Navigator.of(context).pushNamed(route);
        },
      ),
    );
  }
}
