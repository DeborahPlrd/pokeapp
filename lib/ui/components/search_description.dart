import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:poke_app/ui/components/pokemon_specs.dart';

class SearchDescription extends StatelessWidget {
  const SearchDescription(
      this.sprite,
      this.svg,
      this.name,
      this.type,
      this.height,
      this.weight,
      {Key? key}) : super(key: key);
  final String sprite;
  final String svg;
  final String name;
  final String type;
  final int height;
  final int weight;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        (Row(
          children: [
            Image.network(sprite),
            Text(name.toUpperCase(), style: const TextStyle(
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.w700,
                fontSize: 22,
                letterSpacing: 2
            ),)
          ],
        )),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          child: Center(
            child: svg != "" ? SvgPicture.network(
              svg,
              height: 160,
            ): null,
          ),
        ),
        const Divider(
          color: Colors.grey,
          height: 60,
          thickness: 1,
          indent: 20,
          endIndent: 20,
        ),
        PokemonSpecs(type, height, weight),
      ],
    );
  }
}
