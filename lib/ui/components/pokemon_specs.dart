import 'package:flutter/material.dart';

class PokemonSpecs extends StatelessWidget {
  const PokemonSpecs(this.type, this.height, this.weight, {Key? key}) : super(key: key);
  final String type;
  final int height;
  final int weight;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            const Icon(Icons.catching_pokemon, color: Colors.redAccent),
            const Text("TYPE", style: TextStyle(
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.w700,
                fontSize: 17,
                color: Colors.redAccent
            ),),
            Text(type, style: const TextStyle(
              fontFamily: 'Quicksand',
              fontWeight: FontWeight.w600,
              color: Colors.grey,
            ),),
          ],
        ),
        Column(
          children: [
            const Icon(Icons.height, color: Colors.amber),
            const Text("SIZE", style: TextStyle(
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.w700,
                fontSize: 17,
                color: Colors.amber
            ),),
            Text((height/10).toString() + " m", style: const TextStyle(
              fontFamily: 'Quicksand',
              fontWeight: FontWeight.w600,
              color: Colors.grey,
            ),),
          ],
        ),
        Column(
          children: [
            const Icon(Icons.accessibility, color: Colors.lightBlue),
            const Text("WEIGHT", style: TextStyle(
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.w700,
                fontSize: 17,
                color: Colors.lightBlue
            ),),
            Text((weight/10).toString() + " kg", style: const TextStyle(
              fontFamily: 'Quicksand',
              fontWeight: FontWeight.w600,
              color: Colors.grey,
            ),),
          ],
        ),
      ],
    )
    ;
  }
}
