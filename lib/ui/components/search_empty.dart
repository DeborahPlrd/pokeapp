import 'package:flutter/material.dart';

class SearchEmpty extends StatelessWidget {
  const SearchEmpty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 100.0, horizontal: 50),
          child: Text("Search a Pokemon name to see its specs !",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontFamily: 'Quicksand',
                fontWeight: FontWeight.w600,
                fontSize: 22,
                letterSpacing: 2
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset('lib/assets/bulbasaur.png', width: 80,),
            Image.asset('lib/assets/pikachu.png', width: 80,),
            Image.asset('lib/assets/jigglypuff.png', width: 80,),
          ],
        )
      ],
    );
  }
}
