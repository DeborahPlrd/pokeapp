import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:poke_app/ui/components/pokemon_specs.dart';

class DescriptionCard extends StatelessWidget {
  const DescriptionCard(
      this.svg,
      this.name,
      this.type,
      this.height,
      this.weight,
      {Key? key}) : super(key: key);
  final String svg;
  final String name;
  final String type;
  final int height;
  final int weight;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(name.toUpperCase(), style: const TextStyle(
              fontFamily: 'Quicksand',
              fontWeight: FontWeight.w700,
              fontSize: 22,
              letterSpacing: 2
          ),
          ),
          SvgPicture.network(svg, height: 230),
          const Divider(
            color: Colors.grey,
            height: 20,
            thickness: 1,
            indent: 20,
            endIndent: 20,
          ),
          PokemonSpecs(type, height, weight)
        ],
      ),
    );
  }
}
