import 'package:flutter/material.dart';
import 'package:poke_app/blocs/pokemon_cubit.dart';
import 'package:poke_app/models/pokemon.dart';
import 'package:poke_app/repository/pokemon_repository.dart';
import 'package:poke_app/repository/preferences_repository.dart';
import 'package:poke_app/repository/repository.dart';
import 'package:poke_app/ui/components/search_description.dart';
import 'package:poke_app/ui/components/search_empty.dart';
import 'package:provider/provider.dart';

class SearchPokemon extends StatefulWidget {
  const SearchPokemon({Key? key}) : super(key: key);

  @override
  _SearchPokemonState createState() => _SearchPokemonState();
}

class _SearchPokemonState extends State<SearchPokemon> {
  Pokemon? pokemon;
  final List<Pokemon> _pokemons = [];
  late Pokemon _pokemonSpec = Pokemon(0, "", 0, 0, 0, "", "", "");
  final _pokemonRepository = PokemonRepository();
  final _preferencesRepository = PreferencesRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios, color: Colors.black,), onPressed: () => Navigator.of(context).pop(),
        ),
        titleTextStyle: const TextStyle(color: Colors.black, fontSize: 20),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: const Text('Pokédex',
          style: TextStyle(
            fontFamily: 'Quicksand',
            letterSpacing: 2,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          MaterialButton(
            onPressed: () => Navigator.of(context).pushNamed('/home'),
            child: Container(
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('lib/assets/logo.png')
                )
              ),
            ),
          )
        ],
      ),
      body: Center(
        child: Form(

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("lib/assets/pokedexL.png", width: 55,),
                    Container(
                      width: 210,
                      height: 80,
                      child: TextField(
                        onSubmitted: (query) async {
                          pokemon = await Repository(_pokemonRepository, _preferencesRepository).searchPokemons(query.toLowerCase());
                          if(query.isNotEmpty) {
                            setState(() {
                              _pokemons.clear();
                              _pokemons.add(pokemon!);
                              _pokemonSpec = pokemon!;
                            });
                          }
                        },
                        decoration: const InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          labelText: "Enter a Pokemon name",
                          labelStyle: TextStyle(color: Colors.black54),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black)
                          ),
                          border: UnderlineInputBorder(),
                        ),
                        style: const TextStyle(
                          fontFamily: "Quicksand",
                          fontSize: 20,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    Image.asset("lib/assets/pokedexR.png", width: 55,),
                  ],
                ),
              ),
              ListView.builder(
                shrinkWrap: true,
                itemCount: pokemon?.id != 0 ? _pokemons.length : 0,
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      Pokemon pokemon = Pokemon(
                          _pokemons[index].id,
                          _pokemons[index].name,
                          _pokemons[index].order,
                          _pokemons[index].weight,
                          _pokemons[index].height,
                          _pokemons[index].sprite,
                          _pokemons[index].svg,
                          _pokemons[index].type
                      );
                      Provider.of<PokemonCubit>(context, listen: false).addPokemon(pokemon);
                      Navigator.of(context).pushNamed('/my_pokedex', arguments: pokemon);
                      },
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: const [
                        Icon(Icons.favorite, color: Colors.redAccent),
                        Text(" ADD TO TEAM", style: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.w700,
                            fontSize: 17,
                            color: Colors.redAccent
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              _pokemonSpec.id != 0 ?
              SearchDescription(
                  _pokemonSpec.sprite!,
                  _pokemonSpec.svg!,
                  _pokemonSpec.name!,
                  _pokemonSpec.type!,
                  _pokemonSpec.height!,
                  _pokemonSpec.weight!)
                  :
              const SearchEmpty(),
            ],
          ),
        ),
      ),
    );
  }
}
