import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poke_app/blocs/pokemon_cubit.dart';
import 'package:poke_app/models/pokemon.dart';
import 'package:provider/provider.dart';

class MyPokedex extends StatelessWidget {
  const MyPokedex({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios, color: Colors.black,), onPressed: () => Navigator.of(context).pop(),
        ),
        titleTextStyle: const TextStyle(color: Colors.black, fontSize: 20),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: const Text('My team',
          style: TextStyle(
            fontFamily: 'Quicksand',
            letterSpacing: 2,
            fontWeight: FontWeight.w600,
          ),
        ),
        actions: [
          MaterialButton(
            onPressed: () => Navigator.of(context).pushNamed('/home'),
            child: Container(
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('lib/assets/logo.png')
                  )
              ),
            ),
          ),
        ],
      ),
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(image: AssetImage("lib/assets/bg_pokeball.jpg"),
              fit: BoxFit.cover
              )
            ),
            child: BlocBuilder<PokemonCubit, List<Pokemon>>(
              builder: (context, state) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 75.0),
                  child: ListView.builder(
                      itemCount: state.length,
                      itemBuilder: (BuildContext context, int index) {
                        Pokemon pokemon = state[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Card(
                            margin: const EdgeInsets.only(top: 15, bottom: 10,),
                            elevation: 5,
                            child: ListTile(
                              trailing: TextButton(
                                child: const Icon(Icons.close, color: Colors.black,),
                                onPressed: () {
                                  state.removeAt(index);
                                  Provider.of<PokemonCubit>(context, listen: false).savePokemons(pokemon);
                                },
                              ),
                              leading: Image.network(pokemon.sprite!),
                              title: Text(pokemon.name!.toUpperCase(),
                                style: const TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              subtitle: Text(pokemon.type!,
                                style: const TextStyle(
                                  fontFamily: 'Quicksand',
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              onTap: () => Navigator.of(context).pushNamed('/description', arguments: pokemon),
                            ),
                          ),
                        );
                      },
                  ),
                );
              },
            ),
          ),
          Positioned(
            bottom: 0,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.3),
                    spreadRadius: 2,
                    blurRadius: 5
                  )
                ]
              ),
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("lib/assets/emptyBall.png", width: 90,),
                  TextButton(
                    onPressed: () {
                      showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Center(child: Text("CLEAR TEAM",
                              style: TextStyle(
                                  fontFamily: "Quicksand",
                                  fontWeight: FontWeight.w600
                              ),
                            )
                            ),
                            content: const Text("You really want to empty your team ?",
                              textAlign: TextAlign.center,
                              style: TextStyle(fontFamily: "Quicksand"),
                            ),
                            actions: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  TextButton(
                                    onPressed: () => Navigator.pop(context, 'Cancel'),
                                    child: const Text("CANCEL",
                                      style: TextStyle(
                                          fontFamily: "Quicksand",
                                          fontWeight: FontWeight.w600,
                                          color: Colors.redAccent
                                      ),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () => {
                                      Navigator.pop(context, 'Yes'),
                                      Provider.of<PokemonCubit>(context, listen: false).clearPokemons()
                                    },
                                    child: const Text("YES",
                                      style: TextStyle(
                                          fontFamily: "Quicksand",
                                          fontWeight: FontWeight.w600,
                                          color: Colors.lightBlue
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                      );
                    },
                    child: const Text("CLEAR",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontFamily: 'Quicksand',
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
