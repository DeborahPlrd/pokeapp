import 'package:flutter/material.dart';
import 'package:poke_app/ui/components/pokeapp_home_button.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title: const Text("PokéApp",
          style: TextStyle(
            fontFamily: 'Pokemon',
            letterSpacing: 3,
            color: Colors.black,
            fontSize: 32,
          )
        ),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 40.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: const Center(
                  child: Text("Catch'em all !",
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        fontWeight: FontWeight.w700,
                        letterSpacing: 5,
                        color: Colors.black87,
                        fontSize: 25,
                      )),
                ),
              ),
            ),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("lib/assets/bg_pika.jpg"),
                      fit: BoxFit.cover,
                    )
                  ),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: const [
                        PokeappHomeButton("lib/assets/pokedex.png", "POKEDEX", "/search_pokemon"),
                        PokeappHomeButton("lib/assets/team.png", "MY TEAM", "/my_pokedex"),
                      ],
                    ),
                  ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
