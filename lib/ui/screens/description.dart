import 'package:flutter/material.dart';
import 'package:poke_app/ui/components/description_card.dart';
import 'package:poke_app/models/pokemon.dart';

class Description extends StatelessWidget {
  const Description({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Pokemon pokemon = ModalRoute.of(context)?.settings.arguments as Pokemon;

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios, color: Colors.black,), onPressed: () => Navigator.of(context).pop(),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        title: Image.network(pokemon.sprite!, width: 50,),
        actions: [
          MaterialButton(
            onPressed: () => Navigator.of(context).pushNamed('/home'),
            child: Container(
              height: 40,
              width: 40,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('lib/assets/logo.png')
                  )
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: DescriptionCard(pokemon.svg!, pokemon.name!, pokemon.type!, pokemon.height!, pokemon.weight!),
      ),
    );
  }
}
