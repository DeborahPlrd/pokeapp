import 'package:poke_app/models/pokemon.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {
  Future<void> savePokemons(List<Pokemon> pokemons) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> listJson = pokemons.map((Pokemon pokemon) {
      return pokemon.toJson();
    }).toList();
    prefs.setStringList('pokemons', listJson);
  }

  Future<List<Pokemon>> loadPokemons() async {
    SharedPreferences? prefs = await SharedPreferences.getInstance();
    List<String>? prefString = prefs.getStringList("pokemons");
    List<Pokemon> pokemonsSaved = [];
    if (prefString != null) {
      for (var pref in prefString) {
        pokemonsSaved.add(Pokemon.fromFactoJson(pref));
      }
    }
    return pokemonsSaved;
  }

}

