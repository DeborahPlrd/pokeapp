import 'dart:convert';

import 'package:http/http.dart';
import 'package:poke_app/models/pokemon.dart';

class PokemonRepository {
  final Pokemon pokemon = Pokemon(0, "", 0, 0, 0, "", "", "");

  Future<Pokemon> fetchPokemons(String query) async {
    Response response = await get(Uri.https('pokeapi.co', '/api/v2/pokemon/$query'));
    if (response.statusCode == 200) {
      final data = Pokemon.fromJson(jsonDecode(response.body));
      return data;
    } else {
      return pokemon;
    }
  }
}