import 'package:poke_app/models/pokemon.dart';
import 'package:poke_app/repository/pokemon_repository.dart';
import 'package:poke_app/repository/preferences_repository.dart';

class Repository {
  final PreferencesRepository _preferencesRepository;
  final PokemonRepository _pokemonRepository;

  Repository(this._pokemonRepository, this._preferencesRepository);

  Future<Pokemon> searchPokemons(String query) async {
    return PokemonRepository().fetchPokemons(query);
  }

  Future<void> savePokemons(List<Pokemon> pokemons) async {
    _preferencesRepository.savePokemons(pokemons);
  }

  Future<List<Pokemon>> loadPokemons() async => _preferencesRepository.loadPokemons();
}
