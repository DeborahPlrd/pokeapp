import 'package:bloc/bloc.dart';
import 'package:poke_app/models/pokemon.dart';
import 'package:poke_app/repository/repository.dart';

class PokemonCubit extends Cubit<List<Pokemon>> {
  PokemonCubit(this._repository) : super([]);

  final Repository _repository;

  void addPokemon(Pokemon pokemon) {
    emit([...state, pokemon]);
    _repository.savePokemons(state);
  }
  Future<void> loadPokemons() async {
    final List<Pokemon> pokemons = await _repository.loadPokemons();
    emit(pokemons);
  }

  Future<void> savePokemons(Pokemon pokemon) async {
    _repository.savePokemons(state);
    loadPokemons();
  }

  Future<void> clearPokemons() async {
    state.clear();
    emit(state);
    _repository.savePokemons(state);
    loadPokemons();
  }

}