import 'dart:convert';

class Pokemon {
  int? id;
  String? name;
  int? order;
  int? weight;
  int? height;
  String? svg;
  String? sprite;
  String? type;

  String toJson() {
    return jsonEncode({
      'id' : id,
      'name' : name,
      'order' : order,
      'weight' : weight,
      'height' : height,
      'svg' : svg,
      'sprite' : sprite,
      'type' : type,
    });
  }

  Pokemon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    order = json['order'];
    weight = json['weight'];
    height = json['height'];
    Map<String, dynamic> svgs = json['sprites']['other']['dream_world'] ?? {};
    svg = svgs['front_default'];
    Map<String, dynamic> sprites = json['sprites'] ?? {};
    sprite = sprites['front_default'];
    Map<String, dynamic> types = json['types'][0]["type"];
    type = types['name'];
  }

  factory Pokemon.fromFactoJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    return Pokemon(
        map['id'],
        map['name'],
        map['order'],
        map['weight'],
        map['height'],
        map['sprite'],
        map['svg'],
        map['type']
    );
  }


  Pokemon(this.id, this.name, this.order, this.weight, this.height, this.sprite, this.svg, this.type);
}